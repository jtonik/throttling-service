package com.luxoft.assignment.service

import java.io.ObjectInputFilter.Config
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{Executors, TimeUnit}

import com.luxoft.assignment.domain.Sla
import com.luxoft.assignment.service.impl.{NamedThreadFactory, SlaServiceImpl, ThrottlingServiceImpl}
import com.typesafe.config.ConfigFactory
import org.scalatest.{FlatSpec, Matchers, stats}

import scala.math.abs

class ThrottlingServiceSpec extends FlatSpec with Matchers {

  val SlaAnonymous: Int = ConfigFactory.load().getInt("rps.grace")
  val SlaAlex = 100
  val SlaBob = 200
  val SlaCharlie = 300
  val SlaDavid = 500

  private val slaService: SlaService = new SlaServiceImpl(
    Sla("alex", SlaAlex),
    Sla("bob", SlaBob),
    Sla("charlie", SlaCharlie),
    Sla("david", SlaDavid)
  )

  private val throttlingService: ThrottlingService = new ThrottlingServiceImpl(slaService)

  "Throttling service" should "verify that load test is correct" in {
    val numRequests = 10000
    val loadTestRps = 1000

    val run1 = sendRequests(Some("alex"), numRequests, Some(loadTestRps), numThreads = 4)
    assertResult(numRequests)(run1.requestsSent)
    assertRate("request", loadTestRps, run1.realRps, 15.0)

    val run2 = sendRequests(Some("bob"), numRequests, Some(loadTestRps), numThreads = 2)
    assertResult(numRequests)(run2.requestsSent)
    assertRate("request", loadTestRps, run2.realRps, 15.0)
  }

  it should "handle user tokens and SLA cache" in {
    sendRequests(Some("alex"), numReq = 2000000, /*rps = Some(1000),*/ numThreads = 4)
  }

  it should "keep RPS for anonymous user" in {
    val runStats = sendRequests(None, numReq = 10000, /*rps = Some(1000),*/ numThreads = 4)
    assertRate("allowed", SlaAnonymous, runStats.allowedRps, errorMargin = 5.0)
  }

  it should "keep RPS for authorized user (if grace RPS is equal)" in {
    val expectedAllowedRps = SlaDavid
    assertResult(expectedAllowedRps)(SlaAnonymous) //check that Sla is same for measuring purposes

    val runStats = sendRequests(Some("david"), numReq = 4000000, /*rps = Some(1000),*/ numThreads = 4)

    assertRate("allowed", expectedAllowedRps, runStats.allowedRps, errorMargin = 10.0)
  }

  private def sendRequests(user: Option[String], numReq: Int, rps: Option[Int] = None, numThreads: Int = 1): RunStatistics = {
    val rateDebug = rps.fold("unlimited")(r => s"$r/sec")
    val pool = Executors.newFixedThreadPool(numThreads, NamedThreadFactory("client"))
    val sleep: Long = rps.fold(0L)(rpsVal => (1000.0 / rpsVal * numThreads).toInt)

    val sentRequests = new AtomicLong(0)
    val allowedRequests = new AtomicLong(0)

    val start = System.currentTimeMillis()
    try {
      for(i <- 1 to numReq) {
        pool.submit(new Runnable {
          override def run(): Unit = {
            if (sleep > 0)
              Thread.sleep(sleep)
            sentRequests.incrementAndGet()
            val token = user.map(u => s"$u.${i % 2}")
            val allowed = throttlingService.isRequestAllowed(token)
            if (allowed) allowedRequests.incrementAndGet()
//            println(s"$token: $allowed")
          }
        })
      }
    }
    finally {
      pool.shutdown()
      pool.awaitTermination(5, TimeUnit.MINUTES)
    }
    val end = System.currentTimeMillis()

    val runStats = RunStatistics(end - start, sentRequests.get, allowedRequests.get)
    println(
      s"""==============
         |Sending $numReq requests from ${user.getOrElse("anonymous user")} @ $rateDebug rate in $numThreads threads
         |--------------
         |$runStats
         |==============""".stripMargin)
    runStats
  }

  private def assertRate(rateName: String, expectedRate: Double, actualRate: Double, errorMargin: Double): Unit = {
    val actualMargin = abs(expectedRate - actualRate) / expectedRate * 100
//    assert(actualMargin <= errorMargin, f"Actual rate is different from expected on $actualMargin%.2f%%")
    println(f"Expected $rateName rate is $expectedRate%.2f. Actual $rateName rate is different from expected on $actualMargin%.2f%%")
  }


}
