package com.luxoft.assignment.service

case class RunStatistics(
  durationMs  : Long,
  requestsSent: Long,
  requestsAllowed: Long
) {

  def realRps: Double = requestsSent.toDouble / durationMs * 1000
  def averageRequestDurationMs: Double = 1 / realRps * 1000
  def allowedRps: Double = requestsAllowed.toDouble / durationMs * 1000

  override def toString: String =
    f"""Duration: ${durationMs}ms
       #Sent:   $requestsSent   | Avg. RPS:    $realRps%.2f  | Avg. request: $averageRequestDurationMs%.5fms
       #Allowed: $requestsAllowed   | Avg. Allowed RPS: $allowedRps%.2f""".stripMargin('#')
}
