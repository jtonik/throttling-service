package com.luxoft.assignment.service.impl

import com.luxoft.assignment.domain.Sla
import com.luxoft.assignment.service.SlaService
import com.typesafe.scalalogging.StrictLogging

import scala.collection.mutable
import scala.concurrent.Future

class SlaServiceImpl(predefinedSlas : Sla*) extends SlaService with StrictLogging {

  private val rpsByUser = mutable.Map[String, Int]()

  def addSla(sla: Sla): Unit = rpsByUser += sla.user -> sla.rps

  predefinedSlas foreach addSla

  override def getSlaByToken(token: String): Future[Option[Sla]] = {
    val delimiterPos = token.indexOf('.')
    val user = if (delimiterPos > 0) {
      token.substring(0, token.indexOf('.'))
    }
    else {
      token
    }
    val sla = rpsByUser.get(user).map(Sla(user, _))
    Thread.sleep(250)
    logger.debug(s"Aquired SLA: $sla")
    Future.successful(sla)
  }

}
