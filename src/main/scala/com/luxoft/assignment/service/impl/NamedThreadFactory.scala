package com.luxoft.assignment.service.impl

import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicLong

case class NamedThreadFactory(prefix: String) extends ThreadFactory {
  private val threadNumber = new AtomicLong(0L)

  override def newThread(r: Runnable): Thread = new Thread(r, s"$prefix-${threadNumber.incrementAndGet()}")
}
