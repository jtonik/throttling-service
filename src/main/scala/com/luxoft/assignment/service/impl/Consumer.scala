package com.luxoft.assignment.service.impl

import java.util.concurrent.BlockingQueue

abstract class Consumer[T](queue: BlockingQueue[T]) extends Runnable {
  def run(): Unit = {
    while (true) {
      val item = queue.take()
      consume(item)
    }
  }

  def consume(x: T): Unit
}
