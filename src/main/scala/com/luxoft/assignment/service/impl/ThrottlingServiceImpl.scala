package com.luxoft.assignment.service.impl

import java.util.concurrent.{BlockingQueue, ConcurrentHashMap, Executors, LinkedBlockingQueue, ThreadFactory}

import com.luxoft.assignment.domain.Sla
import com.luxoft.assignment.service.{SlaService, ThrottlingService}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.Await

//implicit imports
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class ThrottlingServiceImpl(val slaService: SlaService) extends ThrottlingService with StrictLogging {

  private val UnauthorizedUser = "###" //value that doesn't correlate with tokens

  private val GraceRps = ConfigFactory.load().getInt("rps.grace")

  /** Map of SLA by request token */
  private val slaCache = new ConcurrentHashMap[String, Sla]
  //storing default RPS in cache because it can increase/decrease, so we will apply the same logic for all cases
  slaCache.put(UnauthorizedUser, Sla(UnauthorizedUser, GraceRps))

  /** Queue that stores stores asynchronous requests for retrieving the SLA */
  private val requestSlaQueue = new LinkedBlockingQueue[String]

  /** Stores token buckets by user, not request token */
  private val userBuckets = new ConcurrentHashMap[String, TokenBucket]()

  /**
   * Consumes SLA requests from the queue
   * @param slaQueue incoming requests queue
   */
  class SlaResponseConsumer(slaQueue: BlockingQueue[String]) extends Consumer[String](slaQueue) {
    override def consume(token: String): Unit = doRequestSla(token)
  }

  initSlaQueue()

  override val graceRps: Int = GraceRps

  override def isRequestAllowed(token: Option[String]): Boolean = {
    val requestTime = System.nanoTime()
    val sla = getSla(token)
    val bucket = registerRequest(requestTime, sla)
    logger.debug(s"SLA by token '$token' is $sla. Tokens: ${bucket.tokens}. Allowed: ${bucket.allowed}")
    bucket.allowed
  }

  /**
   * Gets SLA requirements from cache, otherwise submits asynchronous request to [[SlaService]] and returns default SLA.
   * @param token authentication token
   * @return cached SLA or default one if absent
   */
  private def getSla(token: Option[String]): Sla = {
    Option(slaCache.get(token.getOrElse(UnauthorizedUser))) //get persisted SLA for user or anon. anon is always present.
      .getOrElse({ //if not yet present (only for user) - send SLA request and treat user as anon.
        enqueueSlaRequest(token.get)
        slaCache.get(UnauthorizedUser)
      })
  }

  /**
   * Updates requests information required for limiting the requests rate
   * @param timestamp current request's timestamp
   * @param sla SLA limitations for current user
   * @return info about remaining requests quota
   */
  private def registerRequest(timestamp: Long, sla: Sla): TokenBucket = {
    userBuckets.merge(sla.user, TokenBucket.full(sla.rps, timestamp), (b1, b2) => {
//      logger.debug(s"merge: $b1  |  $b2")
      b1.update(timestamp, sla.rps)
    })
  }

  private def initSlaQueue(): Unit = {
    // Submit one consumer per core
    val cores = Runtime.getRuntime.availableProcessors()
    val pool = Executors.newFixedThreadPool(1/*cores*/, NamedThreadFactory("sla-consumer"))

    for (_ <- 1 to cores) {
      pool.submit(new SlaResponseConsumer(requestSlaQueue))
    }
  }

  /**
   * Produces async SLA request
   * @param token authorization token
   */
  private def enqueueSlaRequest(token: String): Unit = {
    if (!requestSlaQueue.contains(token)) {
      logger.debug(s"$token -> Q")
      requestSlaQueue.put(token)
    }
  }

  private def doRequestSla(token: String): Unit = {
    logger.debug(s"Q -> $token")
    val slaInCache = slaService.getSlaByToken(token).map(maybeSla => {
      logger.debug(s"$token -> cache")
      maybeSla.foreach(sla => slaCache.put(token, sla))
    })
    Await.result(slaInCache, 1 minute)
  }

}
