package com.luxoft.assignment.service.impl

import com.luxoft.assignment.service.impl.TokenBucket.{TokensPerRequest, maxTokens, refillTokens}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

import scala.math.{max, min}

/**
 * Structure for storing user's requests count in certain period of time.
 * <br/>Represents a [[https://en.wikipedia.org/wiki/Token_bucket#Algorithm "token bucket"]] algorithm
 *
 * @param tokens number of tokens available in current timeframe. Negative value means that limit is exceeded.
 * @param timestamp timestamp of the latest request
 */
case class TokenBucket(tokens: Int, timestamp: Long, allowed: Boolean) extends StrictLogging {

  def update(timestamp: Long, rps: Int): TokenBucket = {
    val timePassedNs = timestamp - this.timestamp

    val prev = max(this.tokens, 0)
    val refill = refillTokens(timePassedNs, rps)
    // Need to refill tokens for the time passed and then consume 1 token
    var newTokens = min( prev + refill, maxTokens(rps).toLong).toInt
    //trick for avoiding rounding
    val allowed = newTokens >= TokensPerRequest
    if (allowed) {
      newTokens -= TokensPerRequest
    }

    val allowedDebug = if (allowed) s" and taking $TokensPerRequest" else ""
    logger.debug(s"Refilling $prev with $refill tokens for time ${timePassedNs}ms$allowedDebug. New = $newTokens")
    TokenBucket(newTokens, timestamp, allowed)
  }

}

object TokenBucket {

  val RpsUpdateIntervalMs: Int = ConfigFactory.load().getInt("rps.update.interval.ms")
  val TokensPerRequest = 1000

  /**
   * Calculates maximum number of tokens by specified RPS
   * @param rps requests per second limit
   * @return number of tokens available in update interval
   */
  def maxTokens(rps: Int): Int = (rps / 1000.0 * RpsUpdateIntervalMs * TokensPerRequest).toInt

  /**
   * Calculates theoretical number of tokens to be refilled for passed amount of time.
   * May be bigger than maximum allowed amount.
   * @param timeNs time passed in ns
   * @param rps requests per second limit
   * @return amount of tokens to be refilled
   */
  def refillTokens(timeNs: Long, rps: Int): Long = (timeNs / 1000000.0 * rps / 1000.0 * TokensPerRequest).toLong

  def full(rps: Int, timestamp: Long): TokenBucket = {
    new TokenBucket(maxTokens(rps), timestamp, true)
  }

}
