package com.luxoft.assignment.service.impl

import java.util.concurrent.BlockingQueue

abstract class Producer[T](queue: BlockingQueue[T]) extends Runnable {
  def run(): Unit = {
      queue.put(produce())
  }

  def produce(): T
}