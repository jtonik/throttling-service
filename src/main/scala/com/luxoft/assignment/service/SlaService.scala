package com.luxoft.assignment.service

import com.luxoft.assignment.domain.Sla

import scala.concurrent.Future

trait SlaService {

  def getSlaByToken(token:String):Future[Option[Sla]]

}
